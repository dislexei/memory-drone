﻿using UnityEngine;
using System.Collections;

public class HologramScript : MonoBehaviour {
	public GameObject portal;
	private Animation openAnim;
	private AudioSource source;
	private bool talked;
	private bool isOpen;
	private bool stoppedTalking;
	public UiSubtitles _messenger;
	public TeleportScript _teleport;

	// Use this for initialization
	void Start () {
		talked = false;
		isOpen = false;
		stoppedTalking = false;
		source = gameObject.GetComponent<AudioSource> ();
		openAnim = GameObject.Find ("Portal").GetComponent<Animation> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (talked) {
			stoppedTalking = source.isPlaying;
			if (!source.isPlaying) {
				if (!isOpen) {
					openAnim.Play ("Open");	
					isOpen = true;
					_teleport.allowPass = true;
				}
			}

		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (!talked) 
		{
			Debug.Log ("Hologram Triggered");
			_messenger.StartCoroutine ("messageBoard2");
			source.Play ();
			talked = true;
		}
	}
}
