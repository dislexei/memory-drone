﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UiSubtitles : MonoBehaviour {

	private Text subtitler;
	private float[] delay;
	private float[] delay2;
	private string[] message;
	private string []message2;
	float _delay;
	bool messageOneDone;

	// Use this for initialization
	void Start () {
		message = new string[6] {
		"",
		"We have finally found it.",
		"The last piece of the puzzle.",
		"Now we will be able to answer the final question.",
		"Get it..",
		"Get it for us." 
		};
		message2 = new string[6] {
			"",
			"This... object. It holds value for us.",
			"The memories bound within it..",
			"They offer an entry point to a distant past..",
			"Manipulate these memories, make them guide you to it..",
			"So we can give the Final Answer" 
		};
		delay = new float[6] {
			1.0f, 3.0f, 3.0f, 3.0f, 4.0f, 4.5f
		};
		delay2 = new float[6] {
			3.0f, 4.0f, 5.0f, 5.0f, 5.5f, 4.5f
		};
		subtitler = gameObject.GetComponent<Text>();
		//message = subtitler.text;
		_delay = delay[0];
		//StartCoroutine (messageBoard(delay, "is it working?"));
		StartCoroutine ("messageBoard", 2.0f);
	}

	// Update is called once per frame
	void Update () {
		if (!messageOneDone) 
		{
			StartCoroutine ("messageBoard", 2.0f);
		}
	}

	public IEnumerator messageBoard ()
	{
		for (int j = 0; j < message.Length; j++) 
		{
			Debug.Log ("changing message");
			subtitler.text = message[j];
			messageOneDone = true;
			yield return new WaitForSeconds (delay [j]);
		}

		subtitler.text = "";
	}

	public IEnumerator messageBoard2 ()
	{
		float k = 2.0f;
		for (int i = 0; i < message2.Length; i++) 
		{
			Debug.Log ("changing message2");
			subtitler.text = message2[i];
			yield return new WaitForSeconds (delay2 [i]);
		}
		subtitler.text = "";
	}
}
