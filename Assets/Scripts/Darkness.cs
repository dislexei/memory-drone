﻿using UnityEngine;
using System.Collections;




public class Darkness : MonoBehaviour {
	public GameObject toFollow;
	public float distance;
	public GameObject SoundSource;

	float character_position;
	float initial_positionX;
	float initial_positionY;
	float initial_positionZ;

	bool talking;
		
	void Start()
	{
		talking = true;
		initial_positionX = gameObject.transform.position.x;
		initial_positionY = gameObject.transform.position.y;
		initial_positionZ = gameObject.transform.position.z;
		character_position = toFollow.transform.position.x;
		distance = initial_positionX - character_position;
		//Debug.Log ("Distance = " + distance);
	}


	// Update is called once per frame
	void Update () {
		if (talking) {
			character_position = toFollow.transform.position.x;
			//Debug.Log (character_position);
			gameObject.transform.position = new Vector3 (character_position + distance, initial_positionY, initial_positionZ);
			talking = SoundSource.GetComponent<AudioSource> ().isPlaying;
		} else if (gameObject.tag == "ToDestroy"){
			Destroy (gameObject);
		}
	}
}
