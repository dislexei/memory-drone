﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class KeyCodeOpener : MonoBehaviour {
	public GameObject uiKeypad;
	public Component cameraControl;
	public Camera MainCamera;
	public Camera LockCamera;
	public Text messenge;

	private bool active;
	// Use this for initialization
	void Start () {
		active = false;
		cameraControl = GameObject.FindGameObjectWithTag ("Player").GetComponent("FirstPersonController") as MonoBehaviour;
	}
	
	// Update is called once per frame

	void OnTriggerStay(Collider other)
	{
		messenge.text = "Press Q to interact";
		if (other.tag == "Player" && Input.GetKeyDown (KeyCode.Q)) 
		{
			messenge.text = "";
			if (!active) {
				Cursor.visible = true;
				active = true;
				Debug.Log ("E Detected");
				uiKeypad.SetActive (true);
				((Behaviour)cameraControl).enabled = false;
				MainCamera.enabled = false;
				LockCamera.tag = "MainCamera";
				MainCamera.tag = "Temp";

			} else 
			{
				Cursor.visible = false;
				active = false;
				((Behaviour)cameraControl).enabled = true;
				uiKeypad.SetActive (false);
				MainCamera.enabled = true;
				MainCamera.tag = "MainCamera";
				LockCamera.tag = "Temp";
			}
		}
	}
}
