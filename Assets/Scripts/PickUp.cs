﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PickUp : MonoBehaviour {
	public Inventory _inventory;
	public GameObject _soundSystem;
	public Canvas hintsCanvas;
	public Text hint;
	// Use this for initialization
	void Start () {
		//hint = hintsCanvas.GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnTriggerStay(Collider other)
	{
		if(other.tag == "Player"){
			hint.text = "Press Q to collect";


			if(Input.GetKeyDown(KeyCode.Q))
			{
				Debug.Log (gameObject.name + " picked up. " );
				_soundSystem.GetComponent<AudioSource> ().Play ();
				Destroy (gameObject);
				_inventory.addItem (gameObject.name);
				hint.text = "";
				//gameObject.transform.parent = other.GetComponentInChildren<Camera>().transform;
			}
		}
	}

	void OnTriggerExit(Collider other)
	{
		if (other.tag == "Player") {
			hint.text = "";
		}
	}
}
