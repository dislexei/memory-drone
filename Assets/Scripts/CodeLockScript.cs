﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CodeLockScript : MonoBehaviour {

	public Text screenCode;
	public string display;
	string attemptPass;
	string correctPass;
	public GameObject safeDoor;

	public DoorControl _doorControl;
	private AudioSource CodeAudioPlayer;
	public AudioClip ClickAudio;
	public AudioClip SuccessAudio;
	public AudioClip DeniedAudio;

	private bool isOpen;
	public TeleportScript _teleport;


	// Use this for initialization
	void Start () {
		isOpen = false;
		//screenCode = "******!";
		CodeAudioPlayer =gameObject.GetComponent<AudioSource>();
		attemptPass = "";
		correctPass = "8146";
		screenCode.text = display;
		}

	public void appendString (string Character)
	{
		CodeAudioPlayer.PlayOneShot (ClickAudio, 1.0F);
		if (attemptPass.Length < correctPass.Length) {
			attemptPass += Character;
			screenCode.text = attemptPass;
			Debug.Log (attemptPass);
		} else {
			attemptPass = "";
			ValidateString();	
		} 
	}

	public void deleteString()
	{
		CodeAudioPlayer.PlayOneShot (ClickAudio, 1.0F);
		attemptPass = "";
		screenCode.text = display;
	}

	public void callValid()
	{
		ValidateString ();
			}

	public bool ValidateString()
	{
		if (attemptPass == correctPass) {
			Debug.Log ("Access Granted");
			deleteString ();
			screenCode.text = "Access Granted";
			if (!isOpen) {
				_doorControl.OpenDoor ("SafeDoor");
				isOpen = true;
				_teleport.allowPass = true;
			} else {
				safeDoor.GetComponent<Animation> ().Play ("Close");
				isOpen = false;
			}
			//Destroy (safeDoor);
			CodeAudioPlayer.PlayOneShot (SuccessAudio, 1.0F);
			return true;
		} else 
		{
			Debug.Log ("Incorrect Password!");
			deleteString ();
			CodeAudioPlayer.PlayOneShot (DeniedAudio, 1.0F);
			screenCode.text = "Access Denied";
			return false;
		}
	}

}
