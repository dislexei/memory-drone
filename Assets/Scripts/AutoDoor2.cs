﻿using UnityEngine;
using System.Collections;

public class AutoDoor2 : MonoBehaviour {
	Animator anim;
	bool _isOpen;
	private GameObject _door;

	void Start()
	{
		_door = GameObject.Find ("EntryDoor2");
		_isOpen = false;
		anim = _door.GetComponent<Animator> ();
		//anim = gameObject.GetComponentInParent<Animator> ();
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Player") {
			Debug.Log ("Entered");
			_isOpen = true;
			anim.SetBool ("open", _isOpen);
		}
	}

	void OnTriggerExit (Collider other)
	{
		Debug.Log ("Left");
		_isOpen = false;
		anim.SetBool ("open", _isOpen);

	}
}
