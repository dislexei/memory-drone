﻿using UnityEngine;
using System.Collections;

public class OpenScript : MonoBehaviour {
	private bool isOpen;
	private Animation openAnim;

	// Use this for initialization
	void Start () {
		isOpen = false;
		openAnim = gameObject.GetComponent<Animation>();
	}
	
	// Update is called once per frame
	void Update () {
		}

	void OnTriggerEnter(Collider other)
	{
		if (!openAnim.isPlaying && other.tag == "Player") {
			Debug.Log ("Opening Chest");
			if (!isOpen) {
				openAnim.PlayQueued ("Open");
				isOpen = true;
			}
		}
	}

	void OnTriggerExit(Collider other){
		if (!openAnim.isPlaying && other.tag == "Player")  {
			if(isOpen){
					openAnim.PlayQueued ("Close");
					isOpen = false;
				}
		}
	}
}
