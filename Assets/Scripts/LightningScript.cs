﻿using UnityEngine;
using System.Collections;

public class LightningScript : MonoBehaviour {
	public float duration = 1.0F;
	public Light lt;
	// Use this for initialization

	void Start () {
		lt = gameObject.GetComponent<Light> ();
		lt.enabled = true;
	}
	
	// Update is called once per frame
	void Update () {
		float phi = Time.time / duration * 2 * Mathf.PI;
		float amplitude = Mathf.Cos(phi) * 0.5F + 0.5F;
		lt.intensity = amplitude;
	}
}
