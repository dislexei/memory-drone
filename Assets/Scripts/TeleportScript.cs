﻿using UnityEngine;
using System.Collections;

public class TeleportScript : MonoBehaviour {
	public bool allowPass;
	void Start()
	{
		allowPass = false;
	}

	void OnTriggerEnter(Collider other)
	{
		if(allowPass == true)
		{
			Debug.Log ("Next level");
			Application.LoadLevel (Application.loadedLevel + 1);
		}
	}
}
