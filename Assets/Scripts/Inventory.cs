﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Inventory : MonoBehaviour {
	private bool train;
	private bool toy1;
	private bool toy2;

	private List<GameObject> toys;
	public GameObject HandToy1;
	public GameObject HandToy2;
	public GameObject HandToy3;

	public GameObject ActiveObject;

	private int i;
	private int maxToys;
	private int currentToys;
	private bool empty;
	private GameObject temp;

	// Use this for initialization
	void Start () {
		toys = new List<GameObject> ();
		train = false;
		toy1 = false;
		toy2 = false;
		i = 0;
		maxToys = 0;
		empty = true;
	}



	// Update is called once per frame
	void Update () {
		//Debug.Log ("Max toys = " + maxToys);
		if (!empty && ActiveObject == null)
		{
			if (i >= toys.Count ) {
				i = 0;
			} 
			showOne (i);
			ActiveObject = toys [i];
			i++;
		}

		if (Input.GetKeyDown (KeyCode.Tab)) 
		{
			if (!empty) {
				if (i >= toys.Count ) {
					i = 0;
				} 
				hideAll ();
				/*foreach (GameObject item in toys) 
				{
					Debug.Log ("Inventory contains" + item.name + " " + toys.IndexOf(item));
				}*/
				showOne (i);
				//toys [i].GetComponent<MeshRenderer> ().enabled = true;
				ActiveObject = toys[i];
				//Debug.Log ("Active Object = " + ActiveObject.name);
				i++;
			}

		}
	}

	private void hideAll()
	{
		foreach (GameObject toy in toys) {
			toy.GetComponent<MeshRenderer> ().enabled = false;
		}
	}

	private void hideOne(int k)
	{
		toys [k].GetComponent<MeshRenderer> ().enabled = false;
	}

	private void hideOne(string name)
	{
		for (int j = 0; j < toys.Count; j++) 
		{
			if (toys [j].name == name) 
			{
				toys[j].GetComponent<MeshRenderer> ().enabled = false;
			}
		}
	}

	private void showOne(int k)
	{
		Debug.Log ("Number of items on hand = " + toys.Count);
			Debug.Log ("Trying to show item # " + k);
		toys [k].GetComponent<MeshRenderer> ().enabled = true;
	}

	public void addItem(string item)
	{
		switch (item) {
		case "Train":
			train = true;
			toys.Add (HandToy1);
			break;
		case "Ship":
			toy1 = true;
			toys.Add (HandToy2);
			break;
		case "WhirlToy":
			toy2 = true;
			toys.Add (HandToy3);
			break;
		default:
			Debug.Log ("Item " + item + " not found; could not be added");
			break;
		}
		empty = false;
		maxToys += 1;

	}

	public void removeItem(string item)
	{
		maxToys -= 1;
		switch (item) {
		case "train_hand":
			train = false;
			hideOne ("train_hand");
			toys.Remove (HandToy1);

			ActiveObject = null;
			break;
		case "toy1_hand":
			toy1 = false;
			hideOne ("toy1_hand");
			toys.Remove (HandToy2);

			ActiveObject = null;
			break;
		case "toy2_hand":
			toy2 = false;
			hideOne ("toy2_hand");
			toys.Remove (HandToy3);

			break;
		default:
			Debug.Log ("Item " + item + " not found; could not be removed");
			break;
		}
		ActiveObject = null;
		if (maxToys == 0) {
			empty = true;
			ActiveObject = null;
			hideAll ();
		}
	}
}
