﻿using UnityEngine;
using System.Collections;

public class DoorControl : MonoBehaviour{
	// Use this for initialization
	private string doorName;

	public void OpenDoor(string door)
	{
		if (GameObject.Find (door) != null) 
		{
			GameObject.Find (door).GetComponent<Animation> ().Play ("Open");
		} else {
			Debug.Log("Door " + doorName + " not found");
		}
	}

	public void OpenDoor(int doorID)
	{
		doorName = "Door" + doorID;
		if (GameObject.Find (doorName) != null){
			GameObject.Find (doorName).GetComponent<Animation> ().Play ("Open");
		} else {
			Debug.Log("Door " + doorName + " not found");
		}
	}
}