﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChestScript : MonoBehaviour {

	public Inventory _inventory;
	public DoorControl _doorControl;

	private bool toy1;
	private bool toy2;
	private bool toy3;

	private bool toy1_put;
	private bool toy2_put;
	private bool toy3_put;

	public GameObject toy1_obj;
	public GameObject toy2_obj;
	public GameObject toy3_obj;

	public Text messenge;
	public Text mission;

	private int toysFound;

	// Use this for initialization
	void Start () {
		toy1 = false;
		toy2 = false;
		toy3 = false;

		toy1_put = false;
		toy2_put = false;
		toy3_put = false;
		toysFound = 0;
		mission.text = "Collect memories";
	}
	
	// Update is called once per frame
	void Update () {

		if (toy1 && !toy1_put) 
		{
			_doorControl.OpenDoor ("InsideDoor6");
			toy1_obj.GetComponent<MeshRenderer> ().enabled = true;
			toy1_put = true;
		}

		if (toy2 && !toy2_put) 
		{
			_doorControl.OpenDoor ("InsideDoor4");
			toy2_obj.GetComponent<MeshRenderer> ().enabled = true;
			toy2_put = true;
		}

		if (toy3 && !toy3_put) 
		{
			_doorControl.OpenDoor ("InsideDoor3");
			toy3_obj.GetComponent<MeshRenderer> ().enabled = true;
			toy3_put = true;
		}
	
	}

	void OnTriggerStay (Collider other)
	{
		if (other.tag == "Player") 
		{
			if (toysFound < 3) 
			{
				messenge.text = "Find more memories, child";
			}
			//Debug.Log ("Hands Off!");
			if (_inventory.ActiveObject != null)
			{
				messenge.text = "Press Q to place memory into database";
				//Debug.Log ("Put that " + _inventory.ActiveObject.name +" away!");
				if (Input.GetKeyDown(KeyCode.Q))
				{
					string toy_put_name = _inventory.ActiveObject.name;
					switch (toy_put_name)
					{
					case "train_hand":
						toy1 = true;
						Debug.Log ("Toy 1 = " + toy1);
						toysFound += 1;
						mission.text = "Memories collected " + toysFound +"/3";
						break;
					case "toy1_hand":
						toy2 = true;
						Debug.Log ("Toy 1 = " + toy2);
						toysFound += 1;
						mission.text = "Memories collected " + toysFound +"/3";
						break;
					case "toy2_hand":
						toy3 = true;
						Debug.Log ("Toy 1 = " + toy3);
						toysFound += 1;
						mission.text = "Memories collected " + toysFound +"/3";
						break;
					default:
						Debug.Log ("Memory " + toy_put_name + " not found;");
						break;
					}
					_inventory.removeItem (toy_put_name);
					gameObject.GetComponent<AudioSource> ().Play ();
				}
			}

		}
	}

	void OnTriggerExit (Collider other)
	{
		messenge.text = "";

		if (toysFound == 3) 
		{
			mission.text = "Remember the code and bring me knowledge";
		}
	}
}
