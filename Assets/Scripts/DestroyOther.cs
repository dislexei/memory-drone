﻿using UnityEngine;
using System.Collections;

public class DestroyOther : MonoBehaviour {

	public GameObject toDestoy;
	public GameObject DoorToClose;
	public GameObject DoorToOpen;
	private bool firstTime = true;

	void OnTriggerEnter(Collider other)
	{
		if(firstTime)
		{
			Debug.Log ("Middle room triggered");
			Destroy (toDestoy);
			DoorToClose.GetComponent<Animation> ().Play ("Close");
			DoorToOpen.GetComponent<Animation> ().Play ("Open");
			firstTime = false;
			Debug.Log ("Middle room done");

		}
	}
}
