﻿using UnityEngine;
using System.Collections;

public class DoorOpen : MonoBehaviour {
	public GameObject Door;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other){
		Debug.Log ("Door Triggered");
		Destroy (Door.GetComponent<BoxCollider> ());
		Door.GetComponent<Animation> ().Play ("Open");
	}
}
