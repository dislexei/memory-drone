﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Utility;
using UnityStandardAssets.Characters.FirstPerson;

public class MenuScript : MonoBehaviour {
	public Canvas _escMenuCanvas;
	private GameObject Player;
	// Use this for initialization
	void Start () {
		Player = GameObject.FindGameObjectWithTag ("Player");
		if (Application.loadedLevel == 0 || Application.loadedLevel == 3) {
			Cursor.visible = true;
		} else {
			Cursor.visible = false;
		}
	

	}
	
	// Update is called once per frame
	void Update () {
		if (Application.loadedLevel != 0 && Application.loadedLevel !=3) 
		{
			if (Input.GetKeyDown(KeyCode.Escape))
			{
				if (_escMenuCanvas.enabled != true) {
					Cursor.visible = true;
					_escMenuCanvas.enabled = true;
					if (Player != null)
					{
						
						Player.GetComponent<CharacterController> ().enabled = false;
						Player.GetComponent<FirstPersonController> ().enabled = false;
					}

					//Debug.Log ("Opening esc menu");
				} else {
					Cursor.visible = false;
					_escMenuCanvas.enabled = false;
					if (Player != null) {
						Player.GetComponent<CharacterController> ().enabled = true;
						Player.GetComponent<FirstPersonController> ().enabled = true;

					}

				}
			}
		}
	}

	public void Quit()
	{
		Application.Quit ();
	}

	public void NextLevel()
	{
		Application.LoadLevel (Application.loadedLevel + 1);
	}

	public void RestartLevel()
	{
		Application.LoadLevel (Application.loadedLevel);
	}

	public void RestartGame()
	{
		Application.LoadLevel (0);
	}
}
